<?php

namespace app\oauth\controller;

use \think\Request;
use \think\Controller;

class Error extends Controller
{
    public function index(Request $request)
    {
        return jerr("Action Error");
    }
    public function _empty()
    {
        return jerr("Action Error");
    }
}
