<?php

namespace app\oauth\controller;

use app\oauth\Oauth;

class Test extends Oauth
{
    protected $oauthUrl = "/";
    protected $client_id = "1";
    protected $client_secret = "123456";
    public function __construct()
    {
        parent::__construct();
        $this->oauthUrl = $this->request->domain();
    }
    public function index()
    {
        if (input("access_token")) {
            $access_token = input("access_token");
            $ret = httpGetFull($this->oauthUrl . "/oauth/user/getUserinfo/?access_token=" . $access_token);
            $retObj = json_decode($ret);
            if ($retObj->code != 200) {
                $this->login();
            } else {
                print_r($retObj->data);
                echo '<br><hr><br><a href="/oauth/test/login">重新登录</a>';
            }
        } else {
            $this->redirect('/oauth/test/login');
        }
    }
    public function callback()
    {
        if (input("code")) {
            $code = input("code");
            $ret = httpGetFull($this->oauthUrl . "/oauth/accesstoken/?client_id=" . $this->client_id . "&client_secret=" . $this->client_secret . "&code=" . $code);
            $retObj = json_decode($ret);
            if ($retObj->code != 200) {
                $this->login();
            } else {
                $access_token = $retObj->data;
                echo 'access_token is : ' . $access_token . '<br><br><a href="/oauth/test/?access_token=' . $access_token . '">查看个人信息</a>';
            }
        } else {
            echo "Code Missing!";
        }
    }
    public function login()
    {
        $this->redirect($this->oauthUrl . "/oauth/authorize/?client_id=1&redirect=" . urlencode($this->request->domain() . "/oauth/test/callback/"));
    }
}
