<?php

namespace app\oauth\controller;

use app\oauth\Oauth;
use app\model\User as UserModel;

class Authorize extends Oauth
{
	public function index()
	{
		$UserModel = new UserModel();
		//传入参数校验开始
		if (!input("client_id")) {
			echo "client_id missing";
			die();
		}
		if (!input("redirect")) {
			echo "redirect missing";
			die();
		}
		//传入参数校验结束
		$client_id = input("client_id");
		$redirect = input("redirect");
		//校验APP信息
		$app = db("app")->where("app_id", $client_id)->find();
		if (empty($app)) {
			echo "App not found";
			die();
		}
		if ($app['app_status'] == 1) {
			echo "App Not Allowed!";
			die();
		}
		$user = null;
		//登录用户
		if ($this->request->isPost()) {
			//校验参数
			if (!input("post.account")) {
				$this->error('Input your account please!');
				die;
			}
			if (!input("post.password")) {
				$this->error('Input your password please!');
				die;
			}
			$account = input("post.account");
			$password = input("post.password");
			//登录验证
			$user = $UserModel->login($account, $password);
			if (empty($user)) {
				$this->error('Account or password error!');
				die;
			}
			//生成一个临时code
			$code = sha1(time()) . rand(100000, 999999);
			//将之前的code全部设置失效
			db('code')->where('code_user', $user['user_id'])->update([
				'code_status' => 1,
			]);
			//保存新的code
			db("code")->insert([
				'code_user' => $user['user_id'],
				'code_code' => $code,
				'code_createtime' => time(),
				'code_updatetime' => time()
			]);
			//重定向回第三方页面
			$this->redirect(urldecode($redirect) . "?code=" . $code);
			die;
		}
		$this->assign("app", $app);
		return $this->view->fetch();
	}
}
