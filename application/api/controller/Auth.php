<?php
namespace app\api\controller;
use app\api\Api;

use think\Db;

class Auth extends Api{
    /**
     * 删除授权记录
     *
     * @return void
     */
    public function clean()
    {
        //走AccessToken验证
        $error = $this->auth();
        if ($error) {
            return $error;
        }
        // //走Rbac验证
        $error = $this->rbac();
        if ($error) {
            return $error;
        }
        Db::execute("truncate table " . config('database.prefix') . "auth");
        return jok();
    }
}
