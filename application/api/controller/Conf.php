<?php

namespace app\api\controller;

use app\api\Api;

class Conf extends Api
{
    public function __construct()
    {
        //以下字段不允许使用POST修改
        $this->postBlackList = ['conf_id', "conf_status", "conf_createtime", "conf_updatetime", "conf_system"];
        //SQL查询的字段 []为所有
        $this->selectFieldList = [];
        //筛选条件
        $this->searchFilter = [
            "conf_key" => "like", //相似筛选
            "conf_value" => "like", //相似筛选
            "conf_desc" => "like", //相似筛选
        ];
        $this->per_page = 20;
        parent::__construct();
    }
    /**
     * 禁用配置
     *
     * @return void
     */
    public function disable()
    {
        //走AccessToken验证
        $error = $this->auth();
        if ($error) {
            return $error;
        }
        // //走Rbac验证
        $error = $this->rbac();
        if ($error) {
            return $error;
        }
        if (!input($this->pk)) {
            return jerr("禁用失败,缺少参数");
            exit;
        }
        $this->pk_value = input($this->pk);
        $map[$this->pk] = $this->pk_value;
        if (isInteger($this->pk_value)) {
            $item = db($this->table)->where($map)->find();
            if (empty($item)) {
                return jerr("没有这条记录，禁用失败");
                exit;
            }
            if ($item[$this->table . "_system"] == 1) {
                return jerr("系统配置项不允许操作！");
                exit;
            }
        } else {
            $list = explode(',', $this->pk_value);
            $map[$this->pk] = ['in', $list];
            $map[$this->table . '_system'] = 0;
        }
        db($this->table)->where($map)->update([
            $this->table . "_status" => 1,
            $this->table . "_updatetime" => time(),
        ]);
        return jok();
    }

    /**
     * 启动配置
     *
     * @return void
     */
    public function enable()
    {
        //走AccessToken验证
        $error = $this->auth();
        if ($error) {
            return $error;
        }
        // //走Rbac验证
        $error = $this->rbac();
        if ($error) {
            return $error;
        }
        if (!input($this->pk)) {
            return jerr("启用失败,缺少参数");
            exit;
        }
        $this->pk_value = input($this->pk);
        $map[$this->pk] = $this->pk_value;
        if (isInteger($this->pk_value)) {
            $item = db($this->table)->where($map)->find();
            if (empty($item)) {
                return jerr("没有这条记录，启用失败");
                exit;
            }
            if ($item[$this->table . "_system"] == 1) {
                return jerr("系统配置项不允许操作！");
                exit;
            }
        } else {
            $list = explode(',', $this->pk_value);
            $map[$this->pk] = ['in', $list];
            $map[$this->table . '_system'] = 0;
        }
        db($this->table)->where($map)->update([
            $this->table . "_status" => 0,
            $this->table . "_updatetime" => time(),
        ]);
        return jok();
    }

    /**
     * 修改配置
     *
     * @return void
     */
    public function update()
    {
        //走AccessToken验证
        $error = $this->auth();
        if ($error) {
            return $error;
        }
        // //走Rbac验证
        $error = $this->rbac();
        if ($error) {
            return $error;
        }
        if (!input($this->pk)) {
            return jerr("修改失败,缺少参数");
            exit;
        }
        $this->pk_value = input($this->pk);
        if (!isInteger($this->pk_value)) {
            return jerr("修改失败,参数错误");
            exit;
        }
        $map[$this->pk] = $this->pk_value;
        $item = db($this->table)->where($map)->find();
        if (empty($item)) {
            return jerr("没有这条记录，修改失败");
            exit;
        }
        $data = [];
        foreach (input("post.") as $k => $v) {
            if (in_array($k, $this->postBlackList) || in_array($k, $this->postSystem)) {
                continue;
            } else {
                $data[$k] = $v;
            }
        }
        $data[$this->table . "_updatetime"] = time();

        if ($item[$this->table . "_system"] == 1) {
            unset($data[$this->table . "_key"]);
        }
        if ($item[$this->table . "_readonly"] == 1) {
            unset($data[$this->table . "_key"]);
            unset($data[$this->table . "_value"]);
        }
        db($this->table)->where($this->pk, $this->pk_value)->update($data);
        return jok();
    }

    /**
     * 删除配置
     *
     * @return void
     */
    public function delete()
    {
        //走AccessToken验证
        $error = $this->auth();
        if ($error) {
            return $error;
        }
        // //走Rbac验证
        $error = $this->rbac();
        if ($error) {
            return $error;
        }
        if (!input($this->pk)) {
            return jerr("删除失败,缺少参数");
            exit;
        }
        $this->pk_value = input($this->pk);
        $map[$this->pk] = $this->pk_value;
        if (isInteger($this->pk_value)) {
            $item = db($this->table)->where($map)->find();
            if (empty($item)) {
                return jerr("没有这条记录，删除失败");
                exit;
            }
            if ($item[$this->table . "_system"] == 1) {
                return jerr("系统配置项不允许操作！");
                exit;
            }
        } else {
            $list = explode(',', $this->pk_value);
            $map[$this->pk] = ['in', $list];
            $map[$this->table . '_system'] = 0;
        }
        db($this->table)->where($map)->delete();
        return jok();
    }

    /**
     * 读取基本配置
     *
     * @return void
     */
    public function getBaseConfig()
    {
        //走AccessToken验证
        $error = $this->auth();
        if ($error) {
            return $error;
        }
        // //走Rbac验证
        $error = $this->rbac();
        if ($error) {
            return $error;
        }
        $map = [
            "conf_key" => ["in", "app_name,iconfont"]
        ];
        $datalist = db($this->table)->where($map)->order($this->pk . " asc")->select();
        return jok('', $datalist);
    }
    /**
     * 更新基础配置
     *
     * @return void
     */
    public function updateBaseConfig()
    {
        //走AccessToken验证
        $error = $this->auth();
        if ($error) {
            return $error;
        }
        // //走Rbac验证
        $error = $this->rbac();
        if ($error) {
            return $error;
        }
        foreach (input("post.") as $k => $v) {
            $map["conf_key"] = $k;
            $item = db($this->table)->alias($this->table)->where($map)->find();
            if (empty($item)) {
                continue;
            }
            if ($item[$this->table . "_readonly"] == 1) {
                continue;
            }
            db("conf")->where("conf_key", $k)->update(["conf_value" => $v]);
        }
        return jok("配置修改成功");
    }
}
