<?php

namespace app\api\controller;

use app\api\Api;

class Node extends Api
{
	public function __construct()
	{
		//以下字段不允许使用POST修改
		$this->postBlackList = ['node_id', "node_status", "node_createtime", "node_updatetime", "node_system", "subCount"];
		//SQL查询的字段 []为所有
		$this->selectFieldList = [];
		//筛选条件
		$this->searchFilter = [
			"node_title" => "like", //相似筛选
			"node_desc" => "like", //相似筛选
		];
		$this->per_page = 20;
		parent::__construct();
	}
	/**
	 * 获取节点列表接口
	 *
	 * @return void
	 */
	public function lists()
	{
		//走AccessToken验证
		$error = $this->auth();
		if ($error) {
			return $error;
		}
		// //走Rbac验证
		$error = $this->rbac();
		if ($error) {
			return $error;
		}
		$order = $this->table . "_order desc," . $this->pk . " asc";
		$map = [
			"node_pid" => 0
		];
		$datalist = db($this->table)->where($map)->order($order)->select();
		for ($i = 0; $i < count($datalist); $i++) {
			$subMap = [
				"node_pid" => $datalist[$i]['node_id']
			];
			if (input("node_module") && input("node_module") != "__all__") {
				$subMap['node_module'] = input("node_module");
			}
			if (input("node_show") && input("node_show") != "__all__") {
				$subMap['node_show'] = input("node_show");
			}
			if (input("node_controller")) {
				$subMap['node_controller'] = input("node_controller");
			}
			if (input("node_action")) {
				$subMap['node_action'] = input("node_action");
			}
			$field = "*";
			if (count($this->selectFieldList) > 0) {
				$field = $this->pk;
				foreach ($this->selectFieldList as $selectField) {
					$field .= "," . $selectField;
				}
			}
			$subDatalist = db($this->table)->field($field)->where($subMap)->order($order)->select();
			$datalist[$i]['sub'] = $subDatalist;
		}
		return jok('success', [
			'data'  => $datalist,
			'map'   => $map
		]);
	}
	/**
	 * 禁用节点
	 *
	 * @return void
	 */
	public function disable()
	{
		//走AccessToken验证
		$error = $this->auth();
		if ($error) {
			return $error;
		}
		// //走Rbac验证
		$error = $this->rbac();
		if ($error) {
			return $error;
		}
		if (!input($this->pk)) {
			return jerr("禁用失败,缺少参数");
			exit;
		}
		$this->pk_value = input($this->pk);
		$map[$this->pk] = $this->pk_value;
		if (isInteger($this->pk_value)) {
			$item = db($this->table)->where($map)->find();
			if (empty($item)) {
				return jerr("没有这条记录，禁用失败");
				exit;
			}
			if ($item[$this->table . "_system"] == 1) {
				return jerr("系统节点不允许操作！");
				exit;
			}
		} else {
			$list = explode(',', $this->pk_value);
			$map[$this->pk] = ['in', $list];
			$map[$this->table . '_system'] = 0;
		}
		db($this->table)->where($map)->update([
			$this->table . "_status" => 1,
			$this->table . "_updatetime" => time(),
		]);
		return jok();
	}

	/**
	 * 启用节点
	 *
	 * @return void
	 */
	public function enable()
	{
		//走AccessToken验证
		$error = $this->auth();
		if ($error) {
			return $error;
		}
		// //走Rbac验证
		$error = $this->rbac();
		if ($error) {
			return $error;
		}
		if (!input($this->pk)) {
			return jerr("启用失败,缺少参数");
			exit;
		}
		$this->pk_value = input($this->pk);
		$map[$this->pk] = $this->pk_value;
		if (isInteger($this->pk_value)) {
			$item = db($this->table)->where($map)->find();
			if (empty($item)) {
				return jerr("没有这条记录，启用失败");
				exit;
			}
			if ($item[$this->table . "_system"] == 1) {
				return jerr("系统节点不允许操作！");
				exit;
			}
		} else {
			$list = explode(',', $this->pk_value);
			$map[$this->pk] = ['in', $list];
			$map[$this->table . '_system'] = 0;
		}
		db($this->table)->where($map)->update([
			$this->table . "_status" => 0,
			$this->table . "_updatetime" => time(),
		]);
		return jok();
	}

	/**
	 * 显示到菜单中
	 *
	 * @return void
	 */
	public function show_menu()
	{
		//走AccessToken验证
		$error = $this->auth();
		if ($error) {
			return $error;
		}
		// //走Rbac验证
		$error = $this->rbac();
		if ($error) {
			return $error;
		}
		if (!input($this->pk)) {
			return jerr("显示失败,缺少参数");
			exit;
		}
		$this->pk_value = input($this->pk);
		$map[$this->pk] = $this->pk_value;
		if (isInteger($this->pk_value)) {
			$item = db($this->table)->where($map)->find();
			if (empty($item)) {
				return jerr("没有这条记录，显示失败");
				exit;
			}
		} else {
			$list = explode(',', $this->pk_value);
			$map[$this->pk] = ['in', $list];
		}
		db($this->table)->where($map)->update([
			$this->table . "_show" => 1,
			$this->table . "_updatetime" => time(),
		]);
		return jok();
	}

	/**
	 * 从菜单中隐藏
	 *
	 * @return void
	 */
	public function hide_menu()
	{
		//走AccessToken验证
		$error = $this->auth();
		if ($error) {
			return $error;
		}
		// //走Rbac验证
		$error = $this->rbac();
		if ($error) {
			return $error;
		}
		if (!input($this->pk)) {
			return jerr("隐藏失败,缺少参数");
			exit;
		}
		$this->pk_value = input($this->pk);
		$map[$this->pk] = $this->pk_value;
		if (isInteger($this->pk_value)) {
			$item = db($this->table)->where($map)->find();
			if (empty($item)) {
				return jerr("没有这条记录，隐藏失败");
				exit;
			}
		} else {
			$list = explode(',', $this->pk_value);
			$map[$this->pk] = ['in', $list];
		}
		db($this->table)->where($map)->update([
			$this->table . "_show" => 0,
			$this->table . "_updatetime" => time(),
		]);
		return jok();
	}

	/**
	 * 删除节点
	 *
	 * @return void
	 */
	public function delete()
	{
		//走AccessToken验证
		$error = $this->auth();
		if ($error) {
			return $error;
		}
		// //走Rbac验证
		$error = $this->rbac();
		if ($error) {
			return $error;
		}
		if (!input($this->pk)) {
			return jerr("删除失败,缺少参数");
			exit;
		}
		$this->pk_value = input($this->pk);
		$map[$this->pk] = $this->pk_value;
		if (isInteger($this->pk_value)) {
			$item = db($this->table)->where($map)->find();
			if (empty($item)) {
				return jerr("没有这条记录，删除失败");
				exit;
			}
			if ($item[$this->table . "_system"] == 1) {
				return jerr("系统节点不允许操作！");
				exit;
			}
			//删除对应ID的授权记录
			db("auth")->where([
				"auth_node" => $this->pk_value
			])->delete();
		} else {
			$list = explode(',', $this->pk_value);
			$map[$this->pk] = ['in', $list];
			$map[$this->table . '_system'] = 0;
			//删除对应ID的授权记录 批量
			db("auth")->where([
				"auth_node" => ['in', $list]
			])->delete();
		}
		db($this->table)->where($map)->delete();
		return jok();
	}

	/**
	 * 更新节点数据
	 *
	 * @return void
	 */
	public function update()
	{
		//走AccessToken验证
		$error = $this->auth();
		if ($error) {
			return $error;
		}
		// //走Rbac验证
		$error = $this->rbac();
		if ($error) {
			return $error;
		}
		if (!input($this->pk)) {
			return jerr("修改失败,缺少参数");
			exit;
		}
		$this->pk_value = input($this->pk);
		if (!isInteger($this->pk_value)) {
			return jerr("修改失败,参数错误");
			exit;
		}
		$map[$this->pk] = $this->pk_value;
		$item = db($this->table)->where($map)->find();
		if (empty($item)) {
			return jerr("没有这条记录，修改失败");
			exit;
		}
		if (!input("node_title") || !input("node_module")) {
			return jerr("节点名称和模块名称必须填写");
		}
		$data = [];
		foreach (input("post.") as $k => $v) {
			if ( in_array($k, $this->postBlackList) || in_array($k, $this->postSystem)) {
				continue;
			} else {
				$data[$k] = $v;
			}
		}
		$data['node_module'] = strtolower($data['node_module']);
		$data['node_controller'] = input("node_controller") ? strtolower($data['node_controller']) : "";
		$data['node_action'] = input("node_action") ? strtolower($data['node_action']) : "";
		$data[$this->table . "_updatetime"] = time();
		db($this->table)->where($this->pk, $this->pk_value)->update($data);
		return jok();
	}

	/**
	 * 添加一个节点
	 *
	 * @return void
	 */
	public function add()
	{
		//走AccessToken验证
		$error = $this->auth();
		if ($error) {
			return $error;
		}
		// //走Rbac验证
		$error = $this->rbac();
		if ($error) {
			return $error;
		}
		if (!input("node_title") || !input("node_module")) {
			return jerr("节点名称和模块名称必须填写");
		}
		$data = [];
		foreach (input("post.") as $k => $v) {
			if ( in_array($k, $this->postBlackList) || in_array($k, $this->postSystem)) {
				continue;
			} else {
				$data[$k] = $v;
			}
		}
		$data['node_module'] = strtolower($data['node_module']);
		$data['node_controller'] = input("node_controller") ? strtolower($data['node_controller']) : "";
		$data['node_action'] = input("node_action") ? strtolower($data['node_action']) : "";
		$data[$this->table . "_updatetime"] = time();
		$data[$this->table . "_createtime"] = time();
		db($this->table)->insert($data);
		return jok();
	}

	/**
	 * 导入节点
	 *
	 * @return void
	 */
	public function import()
	{
		//走AccessToken验证
		$error = $this->auth();
		if ($error) {
			return $error;
		}
		// //走Rbac验证
		$error = $this->rbac();
		if ($error) {
			return $error;
		}
		$module = "api";
		$dir = "application/" . $module . "/controller";
		$files = scandir($dir);
		foreach ($files as $file) {
			if (is_dir($dir . "/" . $file) || in_array($file, ['Error.php', 'Login.php', 'Logout.php'])) {
				// echo $file.PHP_EOL;
				continue;
			} else {
				$arr = explode(".", $file);
				if (strtolower($arr[count($arr) - 1]) != "php" || count($arr) != 2) {
					continue;
				}
				// echo "api/".$arr[0];
				$class = controller("api/" . $arr[0], 'controller');
				// print_r($class);die;
				$methods = get_methods($class);
				// print_r($methods);die;
				unset($class);
				foreach ($methods as $method) {
					$data = [
						"node_module" => $module,
						"node_controller" => strtolower($arr[0]),
						"node_action" => strtolower($method),
					];
					$exist = db($this->table)->field("node_id")->where($data)->find();
					if (!$exist) {
						$data['node_title'] = $arr[0] . "_" . $method;
						$data['node_module'] = $module;
						$data['node_controller'] = strtolower($arr[0]);
						$data['node_action'] = strtolower($method);
						$data['node_show'] = 0;
						$data['node_createtime'] = time();
						$data['node_updatetime'] = time();
						db($this->table)->insert($data);
					}
				}
			}
		}
		return jok("导入节点成功");
	}
}
