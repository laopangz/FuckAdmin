<?php

namespace app\api\controller;

use app\api\Api;

use think\Db;

class Log extends Api
{
    public function __construct()
    {
        //以下字段不允许使用POST修改
        $this->postBlackList = ['log_id', "log_status", "log_createtime", "log_updatetime", "log_system"];
        //SQL查询的字段 []为所有
        $this->selectFieldList = [];
        //筛选条件
        $this->searchFilter = [
            "log_user" => "=", //相同筛选
            "log_node" => "=", //相同筛选
        ];
        $this->per_page = 20;
        $this->excelField = [
            "id" => "编号",
            "user" => "用户ID",
            "gets" => "GET参数",
            "createtime" => "访问时间",
        ];
        $this->excelTitle = "日志导出数据表";
        parent::__construct();
    }
    /**
     * 清除访问日志
     *
     * @return void
     */
    public function clean()
    {
        //走AccessToken验证
        $error = $this->auth();
        if ($error) {
            return $error;
        }
        // //走Rbac验证
        $error = $this->rbac();
        if ($error) {
            return $error;
        }
        Db::execute("truncate table " . config('database.prefix') . "log");
        return jok();
    }
    /**
     * 获取日志列表
     *
     * @return void
     */
    public function lists()
    {
        //走AccessToken验证
        $error = $this->auth();
        if ($error) {
            return $error;
        }
        // //走Rbac验证
        $error = $this->rbac();
        if ($error) {
            return $error;
        }
        $map = [];
        if (input("per_page")) {
            $this->per_page = intval(input("per_page"));
        }
        if (input("keyword") && input("search")) {
            //存在查询条件
            if (array_key_exists(input("search"), $this->searchFilter)) {
                switch ($this->searchFilter[input("search")]) {
                    case "like":
                        $map[input("search")] = ["like", "%" . urldecode(input("keyword")) . "%"];
                        break;
                    case "=":
                        $map[input("search")] = urldecode(input("keyword"));
                        break;
                    default:
                }
            }
        }
        $order = $this->pk . " desc";
        if (input('order')) {
            $order = input('order');
        }
        $join = [
            ['user user', 'user.user_id = log.log_user', 'left'],
            ['node node', 'node.node_id = log.log_node', 'left'],
        ];

        $field = "*";
        if (count($this->selectFieldList) > 0) {
            $field = $this->pk;
            foreach ($this->selectFieldList as $selectField) {
                $field .= "," . $selectField;
            }
        }
        $datalist = db($this->table)->alias($this->table)->join($join)->field($field)->where($map)->order($order)->paginate($this->per_page, false);
        return jok('success', [
            'list'  => $datalist,
            'map'   => $map
        ]);
    }

    /**
     * 访问统计
     *
     * @return void
     */
    public function state()
    {
        //走AccessToken验证
        $error = $this->auth();
        if ($error) {
            return $error;
        }
        // //走Rbac验证
        $error = $this->rbac();
        if ($error) {
            return $error;
        }
        $join = [
            ["user user", "user.user_id=log.log_user"],
            ["node node", "node.node_id=log.log_node"]
        ];
        $datalist = db(strtolower($this->controller))->alias(strtolower($this->controller))->field("count(log_id) as visitcount,node.*")->join($join)->group("log_node")->order("visitcount desc")->select();
        return jok('', $datalist);
    }
}
