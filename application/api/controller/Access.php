<?php
namespace app\api\controller;
use app\api\Api;

class Access extends Api{
	public function __construct()
	{
	    //以下字段不允许使用POST修改
		$this -> postBlackList = ['access_id', "access_status", "access_createtime", "access_updatetime", "access_system", "access_subCount"];
    	//SQL查询的字段 []为所有
    	$this -> selectFieldList = [];
		//筛选条件
		$this -> searchFilter = [
		    "access_id"=>"=",//相同筛选
		    "access_id"=>"like",//相似筛选
		];
		$this -> per_page = 20;
		parent::__construct();
	}
}
