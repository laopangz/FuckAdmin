<?php

namespace app\api\controller;

use app\api\Api;

class Group extends Api
{
    public function __construct()
    {
        //以下字段不允许使用POST修改
        // $this->postBlackList = ['user_id', "user_status", "user_createtime", "user_updatetime"];
        //以下字段查询时不会被返回
        // $this->selectFieldList = ["user_name", "user_status"];
        //筛选条件
        $this->searchFilter = [
            "group_id" => "=", //相同筛选
            "group_name" => "like",//相似筛选
        ];
        $this->per_page = 20;
        parent::__construct();
    }
	/**
	 * 禁用用户组
	 *
	 * @return void
	 */
	public function disable()
	{
		//走AccessToken验证
		$error = $this->auth();
		if ($error) {
			return $error;
		}
		//走Rbac验证
		$error = $this->rbac();
		if ($error) {
			return $error;
		}
		if (!input($this->pk)) {
			return jerr("禁用失败,缺少参数");
			exit;
		}
		$this->pk_value = input($this->pk);
		$map[$this->pk] = $this->pk_value;
		if (isInteger($this->pk_value)) {
			$item = db($this->table)->where($map)->find();
			if (empty($item)) {
				return jerr("没有这条记录，禁用失败");
				exit;
			}
			if ($item[$this->table . "_system"] == 1) {
				return jerr("系统用户组不允许操作！");
				exit;
			}
		} else {
			$list = explode(',', $this->pk_value);
			$map[$this->pk] = ['in', $list];
			$map[$this->table . '_system'] = 0;
		}
		db($this->table)->where($map)->update([
			$this->table . "_status" => 1,
			$this->table . "_updatetime" => time(),
		]);
		return jok();
	}

	/**
	 * 启用用户组
	 *
	 * @return void
	 */
	public function enable()
	{
		//走AccessToken验证
		$error = $this->auth();
		if ($error) {
			return $error;
		}
		//走Rbac验证
		$error = $this->rbac();
		if ($error) {
			return $error;
		}
		if (!input($this->pk)) {
			return jerr("启用失败,缺少参数");
			exit;
		}
		$this->pk_value = input($this->pk);
		$map[$this->pk] = $this->pk_value;
		if (isInteger($this->pk_value)) {
			$item = db($this->table)->where($map)->find();
			if (empty($item)) {
				return jerr("没有这条记录，启用失败");
				exit;
			}
			if ($item[$this->table . "_system"] == 1) {
				return jerr("系统用户组不允许操作！");
				exit;
			}
		} else {
			$list = explode(',', $this->pk_value);
			$map[$this->pk] = ['in', $list];
			$map[$this->table . '_system'] = 0;
		}
		db($this->table)->where($map)->update([
			$this->table . "_status" => 0,
			$this->table . "_updatetime" => time(),
		]);
		return jok();
	}

	/**
	 * 删除用户组
	 *
	 * @return void
	 */
	public function delete()
	{
		//走AccessToken验证
		$error = $this->auth();
		if ($error) {
			return $error;
		}
		//走Rbac验证
		$error = $this->rbac();
		if ($error) {
			return $error;
		}
		if (!input($this->pk)) {
			return jerr("删除失败,缺少参数");
			exit;
		}
		$this->pk_value = input($this->pk);
		$map[$this->pk] = $this->pk_value;
		if (isInteger($this->pk_value)) {
			$item = db($this->table)->where($map)->find();
			if (empty($item)) {
				return jerr("没有这条记录，删除失败");
				exit;
			}
			if ($item[$this->table . "_system"] == 1) {
				return jerr("系统用户组不允许操作！");
				exit;
			}
			//删除对应ID的授权记录
			db("auth")->where([
				"auth_group" => $this->pk_value
			])->delete();
		} else {
			$list = explode(',', $this->pk_value);
			$map[$this->pk] = ['in', $list];
			$map[$this->table . '_system'] = 0;
			//删除对应用户组的授权记录
			db("auth")->where([
				"auth_group" => ['in', $list]
			])->delete();
		}
		db($this->table)->where($map)->delete();
		return jok();
	}
	/**
	 * 为用户组授权节点
	 *
	 * @return void
	 */
	public function authorize()
	{
		//走AccessToken验证
		$error = $this->auth();
		if ($error) {
			return $error;
		}
		//走Rbac验证
		$error = $this->rbac();
		if ($error) {
			return $error;
		}
		if (!input($this->pk)) {
			return jerr("修改失败,缺少参数");
			exit;
		}
		$this->pk_value = input($this->pk);
		if (!isInteger($this->pk_value)) {
			return jerr("修改失败,参数错误");
			exit;
		}
		$map[$this->pk] = $this->pk_value;
		$item = db($this->table)->where($map)->find();
		if (empty($item)) {
			return jerr("用户组信息查询失败，授权失败");
			exit;
		}
		db("auth")->where([
			"auth_group" => $this->pk_value
		])->delete();
		if ($item[$this->pk] == 1) {
			return jerr("超级管理组无需授权！");
			exit;
		}
		$node_ids = explode(",", input("node_ids"));
		foreach ($node_ids as $node_id) {
			if (intval($node_id) == 0) {
				continue;
			}
			db("auth")->insert([
				"auth_group" => $this->pk_value,
				"auth_node" => $node_id,
				"auth_createtime" => time(),
				"auth_updatetime" => time()
			]);
		}
		return jok();
	}
	/**
	 * 获取用户组拥有的权限
	 *
	 * @return void
	 */
	public function getAuthorize()
	{
		//走AccessToken验证
		$error = $this->auth();
		if ($error) {
			return $error;
		}
		//走Rbac验证
		$error = $this->rbac();
		if ($error) {
			return $error;
		}
		if (!input($this->pk)) {
			return jerr("修改失败,缺少参数");
			exit;
		}
		$this->pk_value = input($this->pk);
		if (!isInteger($this->pk_value)) {
			return jerr("修改失败,参数错误");
			exit;
		}
		$map[$this->pk] = $this->pk_value;
		$item = db($this->table)->where($map)->find();
		if (empty($item)) {
			return jerr("用户组信息查询失败，授权失败");
			exit;
		}
		$myAuthorizeList = $this->AuthModel->where("auth_group", $this->pk_value)->select();
		return jok('ok', $myAuthorizeList);
	}
}
