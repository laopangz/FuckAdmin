<?php

namespace app\api\controller;

class Error
{
    public function _empty()
    {
        return jerr("没有找到请求的API资源", 404);
    }
}
