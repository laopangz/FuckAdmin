<?php

namespace app\api\controller;

use app\api\Api;
use app\model\Attach as AttachModel;

class Attach extends Api
{
    protected $AttachModel;
    public function __construct()
    {
        //以下字段不允许使用POST修改
        // $this -> postBlackList = ['access_id', "access_status", "access_createtime", "access_updatetime", "access_system", "access_subCount"];
        //SQL查询的字段 []为所有
        // $this -> selectFieldList = [];
        //筛选条件
        // $this -> searchFilter = [
        //     "access_id"=>"=",//相同筛选
        //     "access_id"=>"like",//相似筛选
        // ];
        $this->per_page = 20;
        parent::__construct();
        $this->AttachModel = new AttachModel();
    }
    /**
     * 提问中附件提交相关
     *
     * @return void
     */
    public function uploadImage()
    {
        //走AccessToken验证
        $error = $this->auth();
        if ($error) {
            return $error;
        }
        // //走Rbac验证
        $error = $this->rbac();
        if ($error) {
            return $error;
        }
        // 获取表单上传文件 例如上传了001.jpg
        $file = request()->file('image');
        // 移动到框架应用根目录/public/uploads/ 目录下
        $info = $file->validate([
            'size' => intval(config("upload_max_image")),
            'ext' => config("upload_image_type"),
        ])->rule('md5')->move(ROOT_PATH . '/uploads');

        if ($info) {
            // 成功上传后 获取上传信息
            $image_name = '/uploads/' . $info->getSaveName();

            // 新的附件表数据创建
            $attach_data = array(
                'attach_path' => $image_name,
                'attach_type' => 'image',
                'attach_size' => $info->getSize(),
                'attach_createtime' => time(),
                'attach_user' => $this->user['user_id']
            );
            $attach_id = $this->AttachModel->insertGetId($attach_data);
            $attach_data = $this->AttachModel->where(["attach_id" => $attach_id])->find();
            if (input("?extend")) {
                $attach_data['extend'] = input("extend");
            }
            return jok('上传成功！', $attach_data);
        } else {
            // 上传失败获取错误信息
            $error =  $file->getError();
        }
        return jerr($error);
    }
    public function uploadFile()
    {
        //走AccessToken验证
        $error = $this->auth();
        if ($error) {
            return $error;
        }
        // //走Rbac验证
        $error = $this->rbac();
        if ($error) {
            return $error;
        }
        // 获取表单上传文件 例如上传了001.jpg
        $file = request()->file('file');
        // 移动到框架应用根目录/public/uploads/ 目录下
        $info = $file->validate([
            'size' => intval(config("upload_max_file")),
            'ext' => config("upload_file_type"),
        ])->rule('md5')->move(ROOT_PATH . '/uploads');

        if ($info) {
            // 成功上传后 获取上传信息
            $image_name = '/uploads/' . $info->getSaveName();

            // 新的附件表数据创建
            $attach_data = array(
                'attach_path' => $image_name,
                'attach_type' => $info->getExtension(),
                'attach_size' => $info->getSize(),
                'attach_createtime' => time(),
                'attach_user' => $this->user['user_id']
            );
            $attach_id = $this->AttachModel->insertGetId($attach_data);
            $attach_data = $this->AttachModel->where(["attach_id" => $attach_id])->find();
            if (input("?extend")) {
                $attach_data['extend'] = input("extend");
            }
            return jok('上传成功！', $attach_data);
        } else {
            // 上传失败获取错误信息
            $error =  $file->getError();
        }
        return jerr($error);
    }
    public function delete()
    {
        //走AccessToken验证
        $error = $this->auth();
        if ($error) {
            return $error;
        }
        // //走Rbac验证
        $error = $this->rbac();
        if ($error) {
            return $error;
        }
        $this->pk_value = input($this->pk);
        $map[$this->pk] = $this->pk_value;
        if (!isInteger($this->pk_value)) {
            $list = explode(',', $this->pk_value);
            $map[$this->pk] = ['in', $list];
        }
        $attachs = db($this->table)->where($map)->select();
        foreach ($attachs as $attach) {
            $attach['attach_path'] = "." . $attach['attach_path'];
            if (file_exists($attach['attach_path'])) {
                unlink($attach['attach_path']);
            }
        }
        db($this->table)->where($map)->delete();
        return jok();
    }
}
