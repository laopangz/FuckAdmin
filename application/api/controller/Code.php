<?php
namespace app\api\controller;
use app\api\Api;

class Code extends Api{
	public function __construct()
	{
	    //以下字段不允许使用POST修改
		$this -> postBlackList = ['code_id', "code_status", "code_createtime", "code_updatetime", "code_system", "code_subCount"];
    	//SQL查询的字段 []为所有
    	$this -> selectFieldList = [];
		//筛选条件
		$this -> searchFilter = [
		    "code_id"=>"=",//相同筛选
		    "code_id"=>"like",//相似筛选
		];
		$this -> per_page = 20;
		parent::__construct();
	}
}
