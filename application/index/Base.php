<?php
namespace app\index;
use \think\Request;
use \think\View;
use \think\Controller;
class Base extends Controller{
	protected $access_token;
	protected $view;
	protected $request;
	protected $controller;
	protected $action;
    protected $validList=[];
	public function __construct() {
        $this->init();
	}
	
	public function _empty($name)
    {
        return $this->show();
    }
    protected function init(){
        $this->view=new view();
        $this->request=Request::instance();
	    $this->controller=$this->request->controller();
	    $this->action=strtolower($this->request->action());
    }
    protected function show(){
        return $this->view->fetch();
    }
}
