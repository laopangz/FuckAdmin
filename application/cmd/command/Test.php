<?php

namespace app\cmd\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\Loader;

class Test extends Command
{
    protected function configure()
    {
        //可选 设置名称与描述
        $this->setName('test')->setDescription('this is description');
    }
    //具体的执行方法
    protected function execute(Input $input, Output $output)
    {
        echo 'Test command run success!' . PHP_EOL;
    }
}
