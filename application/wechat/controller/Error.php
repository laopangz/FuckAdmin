<?php

namespace app\wechat\controller;

use app\wechat\Base;

class Error extends Base
{
    public function index()
    {
        $this->error("控制器不存在");
    }
}
