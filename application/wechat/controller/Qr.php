<?php

namespace app\wechat\controller;

use app\wechat\Base;

class Qr extends Base
{
	protected function temp($data, $ticket = "")
	{
		$data = urldecode($data);
		if ($ticket != sha1("temp" . urlencode($data)) && $ticket != "Hamm.cn") {
			echo "Ticket Error!";
			exit;
		}
		header('Content-type: image/jpg');
		$data = urlencode($data);
		$data = '{"expire_seconds": 2592000, "action_name": "QR_STR_SCENE", "action_info": {"scene": {"scene_str": "' . $data . '"}}}';
		$ret = httpPost("https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=" . $this->access_token, $data);
		$ret = json_decode($ret);
		header("Location: https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=" . urlencode($ret->ticket));
	}
	protected function last($data, $ticket = "")
	{
		$data = urldecode($data);
		if ($ticket != sha1("last" . urlencode($data)) && $ticket != "Hamm.cn") {
			echo "Ticket Error!";
			exit;
		}
		header('Content-type: image/jpg');
		$data = urlencode($data);
		$data = '{"action_name": "QR_LIMIT_STR_SCENE", "action_info": {"scene": {"scene_str": "' . $data . '"}}}';
		$ret = httpPost("https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=" . $this->access_token, $data);
		$ret = json_decode($ret);
		header("Location: https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=" . urlencode($ret->ticket));
	}
}
