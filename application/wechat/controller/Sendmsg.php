<?php

namespace app\wechat\controller;

use app\wechat\Base;

class Sendmsg extends Base
{
    public function text($openid, $msg = "Hello Hamm.cn!")
    {
        $data = '{"touser":"' . $openid . '","msgtype":"text","text":{"content":"' . $msg . '"}}';
        $ret = httpPost("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" . $this->access_token, $data);
        print_r($ret);
    }

    public function template($openid, $msg = "msg missing", $title = "title missing", $content = "content missing", $url = "")
    {
        $data = ' {
                    "touser":"' . $openid . '",
                    "template_id":"sE0P3JxFX-Askvyd8KTmUDdLewt-j7J5C-spvDEDAy4",
                    "url":"' . $url . '",    
                    "data":{
                        "first": {
                            "value":"' . $msg . '",
                            "color":"#173177"
                        },
                        "keyword1":{
                            "value":"' . $title . '",
                            "color":"#173177"
                        },
                        "keyword2": {
                            "value":"' . date("m-d H:i") . '",
                            "color":"#173177"
                        },
                        "remark":{
                            "value":"' . $content . '",
                            "color":"#173177"
                        }
                    }
                }';

        $ret = $this->httpPost("https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" . $this->access_token, $data);
        print_r($ret);
    }
}
