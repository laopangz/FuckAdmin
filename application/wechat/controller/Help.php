<?php

namespace app\wechat\controller;

use app\wechat\Base;

class Help extends Base
{
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		echo '<a href="/wechat/help/list/" style="color:#333;text-decoration:none;">User List</a>　　';
		echo '<a href="/wechat/help/menu/" style="color:#333;text-decoration:none;">Menu List</a>　　';
	}
	public function info($openid)
	{
		$this->index();
		$ret = httpPost("https://api.weixin.qq.com/cgi-bin/user/info?access_token=" . $this->access_token . "&openid=" . $openid);
		echo "<hr>";
		$ret = json_decode($ret);
		echo '<img src="' . $ret->headimgurl . '" width="100px" height="100px"/><br>';
		echo $ret->nickname . "<br>";
		echo $ret->sex == 1 ? "男<br>" : "女<br>";
		echo $ret->province . " " . $ret->city . "<br>";
		echo date('Y-m-d H:i:s', $ret->subscribe_time) . "<br>";
	}
	public function menu()
	{
		$this->index();

		$ret = httpPost("https://api.weixin.qq.com/cgi-bin/menu/get?access_token=" . $this->access_token);
		echo "<hr>";
		$ret = json_decode($ret);
		print_r(json_encode($ret->menu));
	}
	public function list()
	{
		$this->index();

		$ret = httpPost("https://api.weixin.qq.com/cgi-bin/user/get?access_token=" . $this->access_token);
		echo "<hr>";
		$ret = json_decode($ret);
		$userlist = $ret->data->openid;
		foreach ($userlist as $user) {
			echo '<font style="width:330px;color:#999;display:inline-block;">' . $user . '</font><a href="/wechat/help/info/?openid=' . $user . '" style="color:#333;text-decoration:none;">Info</a>　　<a href="/wechat/sendmsg/text/?openid=' . $user . '" style="color:#333;text-decoration:none;">Text</a>　　<a href="/wechat/sendmsg/template/?openid=' . $user . '" style="color:#333;text-decoration:none;">Template</a>　　<hr>';
		}
	}
}
