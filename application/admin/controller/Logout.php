<?php

namespace app\admin\controller;

use app\admin\Base;
use \think\Cookie;

class Logout extends Base
{
    public function __construct()
    { }

    /**
     * 管理员退出
     *
     * @param string $callback
     * @return void
     */
    public function index($callback = "")
    {
        Cookie::delete('access_token');
        $this->redirect("/admin/login");
    }
}
