<?php

namespace app\admin\controller;

use app\admin\Base;

class Group extends Base
{
	/**
	 * 渲染用户组授权页面
	 *
	 * @return void
	 */
	public function authorize()
	{
		if (!input($this->pk)) {
			return jerr("操作失败,缺少参数");
			exit;
		}
		$this->pk_value = input($this->pk);
		$this->assign($this->pk, $this->pk_value);
		return $this->show();
	}
}
