<?php

namespace app\admin\controller;

use app\admin\Base;

class Login extends Base
{
    public function __construct()
    {
        $configs = db("conf")->select();
        $c = [];
        foreach ($configs as $config) {
            $c[$config['conf_key']] = $config['conf_value'];
        }
        config($c);
        $this->init();
        $this->assign("module", $this->module);
        $this->assign("controller", $this->controller);
        $this->assign("action", $this->action);
    }
    public function index()
    {
        $callback = "/admin";
        if(input("callback")){
            $callback = urldecode(input("callback"));
        }
        $this->assign("callback",$callback);
        return $this->show();
    }
}
