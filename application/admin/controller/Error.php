<?php

namespace app\admin\controller;

use app\admin\Base;

class Error extends Base
{
    /**
     * 捕获控制器不存在的事件
     *
     * @param [string] $name
     * @return void
     */
    public function _empty($name)
    {
        return $this->show();
    }
}
