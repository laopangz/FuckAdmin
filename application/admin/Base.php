<?php

namespace app\admin;

use \think\Request;
use \think\View;
use \think\Controller;
use app\model\Conf as ConfModel;
use app\model\User as UserModel;
use app\model\Group as GroupModel;
use app\model\Node as NodeModel;
use app\model\Auth as AuthModel;
use app\model\Log as LogModel;

class Base extends Controller
{
	protected $view;
	protected $request;
	protected $module;
	protected $controller;
	protected $action;
	//用户信息
	protected $user = null;
	//用户组
	protected $group = null;
	//以下字段不允许POST修改
	protected $postBlackList = [];
	//SQL查询的字段
	protected $selectFieldList = [];
	//搜索字段
	protected $searchFilter = [];
	//默认页面分页条数
	protected $per_page = 20;
	//主键key
	protected $pk = '';
	//表名称
	protected $table = '';
	//主键value
	protected $pk_value = 0;
	//开始引入Model
	protected $ConfModel;
	protected $UserModel;
	protected $GroupModel;
	protected $NodeModel;
	protected $AuthModel;
	protected $LogModel;
	public function __construct()
	{
		//初始化model
		$this->ConfModel = new ConfModel();
		$this->UserModel = new UserModel();
		$this->GroupModel = new GroupModel();
		$this->NodeModel = new NodeModel();
		$this->AuthModel = new AuthModel();
		$this->LogModel = new LogModel();
		//初始化配置
		$configs = $this->ConfModel->select();
		$c = [];
		foreach ($configs as $config) {
			$c[$config['conf_key']] = $config['conf_value'];
		}
		config($c);
		//整体初始化
		$this->init();
		//管理端用户验证
		$this->auth();
		$this->assign("module", $this->module);
		$this->assign("controller", $this->controller);
		$this->assign("action", $this->action);

		//初始化获取pk table searchFilter等字段数据
		$this->pk = strtolower($this->controller) . "_id";
		$this->table = strtolower($this->controller);
		if (empty($this->searchFilter)) {
			$this->searchFilter = [$this->pk];
		}
	}
	/**
	 * 捕获没有声明的方法
	 *
	 * @return void
	 */
	public function _empty($name)
	{
		return $this->show();
	}

	/**
	 * 初始化请求数据
	 */
	protected function init()
	{
		$this->view = new view();
		//获取全局Request
		$this->request = Request::instance();
		//获取module controller action
		$this->module = strtolower($this->request->module()) ? strtolower($this->request->module()) : "admin";
		$this->controller = $this->request->controller() ? $this->request->controller() : "Index";
		$this->action = strtolower($this->request->action()) ? strtolower($this->request->action()) : "index";
	}

	/**
	 * 渲染页面
	 * @return mixed
	 */
	protected function show()
	{
		return $this->view->fetch();
	}

	/**
	 * 用户身份验证 RBAC
	 */
	protected function auth()
	{
		$access_token = cookie('access_token');
		$this->assign("access_token", $access_token);
		$callback = "/admin";
		if (strtolower($this->controller) != "index") {
			$callback .= "/" . strtolower($this->controller);
		}
		if ($this->action != "index") {
			$callback .= "/" . $this->action;
		}
		if (!$access_token) {
			$this->redirect("/admin/login/?callback=" . urlencode($callback));
			die;
		}
		$this->user = $this->UserModel->getUserByAccessToken($access_token);
		if (!$this->user) {
			$this->redirect("/admin/login/?callback=" . urlencode($callback));
			die();
		}
		if ($this->user['user_status']  > 0) {
			$this->error("抱歉，你的帐号已被禁用，暂时无法登录系统！");
		}
		cookie("access_token", $access_token);
		$this->assign('userInfo', $this->user);
		$this->group = $this->GroupModel->getGroupById($this->user['user_group']);
		if ($this->group) {
			if ($this->group['group_id'] != 1 && $this->group['group_status'] == 1) {
				$this->error("抱歉，你所在的用户组已被禁用，暂时无法登录系统");
				die;
			} else {
				$this->assign("menuList", $this->AuthModel->getAdminMenuListByUserId($this->group['group_id']));
				$node = $this->NodeModel->getNodeByUrl($this->module, strtolower($this->controller), $this->action);
				$this->assign('node', $node);
			}
		} else {
			$this->error("抱歉，没有查到你的用户组信息，暂时无法登录系统");
			die;
		}
	}
	/**
	 * 通用修改页面渲染
	 *
	 * @return void
	 */
	public function update()
	{
		if (!input($this->pk)) {
			return jerr( "操作失败,缺少参数");
			exit;
		}
		$this->pk_value = input($this->pk);
		$this->assign($this->pk,$this->pk_value);
		return $this->show();
	}
}
