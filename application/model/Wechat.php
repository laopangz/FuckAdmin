<?php

namespace app\model;

use think\Model;

class Wechat extends Model
{
    /**
     * 通过OPENID获取微信用户
     *
     * @param string OPENID
     * @return wechat
     */
    public function getWechatByOpenid($openid)
    {
        $wechat = $this->where('wechat_openid', $openid)->find();
        return $wechat ? $wechat : false;
    }

    /**
     * 通过ID获取微信用户
     *
     * @param string OPENID
     * @return wechat
     */
    public function getWechatById($openid)
    {
        $wechat = $this->where('wechat_id', $openid)->find();
        return $wechat ? $wechat : false;
    }
}
