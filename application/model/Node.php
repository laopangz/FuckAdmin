<?php
namespace app\model;
use think\Model;

class Node extends Model{
    /**
     * 根据模块/控制器/方法 获取节点信息
     *
     * @param string 模块名称
     * @param string 控制器名称
     * @param string 方法名称
     * @return mixed
     */
    public function getNodeByUrl($module,$controller,$action){
        $node = $this -> where([
            "node_module" => $module,
            "node_controller" => $controller,
            "node_action" => $action
        ])->find();
        if($node){
            return $node;
        }else{
            return false;
        }
    }
}