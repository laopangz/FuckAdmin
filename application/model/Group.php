<?php
namespace app\model;
use think\Model;

class Group extends Model{
    /**
     * 获取指定ID的用户组
     *
     * @param int 组ID
     * @return void
     */
    public function getGroupById($id){
        $group = $this -> where([
            "group_id" => $id
        ])->find();
        if($group){
            return $group;
        }else{
            return false;
        }
    }
}