<?php

namespace app\web\controller;

use app\web\Base;

class Index extends Base
{
    public function _empty()
    {
        $this->login();
        return $this->show();
    }
}
