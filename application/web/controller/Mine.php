<?php

namespace app\web\controller;

use app\web\Base;

class Mine extends Base
{
    public function _empty()
    {
        $this->login();
        return $this->show();
    }
}
