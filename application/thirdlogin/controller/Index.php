<?php

namespace app\thirdlogin\controller;

use app\thirdlogin\Thirdlogin;

class Index extends Thirdlogin
{
	public function index()
	{
		$callback = urldecode(input("callback"));
		cookie("callback", $callback);
		$this->view->assign('callback', $callback);
		return $this->view->fetch();
	}
}
