<?php

namespace app\thirdlogin\controller;
use app\thirdlogin\Thirdlogin;

class Qq extends Thirdlogin
{
	public $appid = "";
	public $appkey = "";
	public $oauthUrl = "https://graph.qq.com/oauth2.0/authorize";
	public $accesstokenUrl = "https://graph.qq.com/oauth2.0/token";
	public $openidUrl = "https://graph.qq.com/oauth2.0/me";
	public $userinfoUrl = "https://graph.qq.com/user/get_user_info";
	public function index()
	{
		$callback = urldecode(input("callback"));
		cookie("callback", $callback);
		$this->login();
		die;
	}
	public function login()
	{
		$this->redirect($this->oauthUrl . '?client_id=' . $this->appid . "&response_type=code&scope=get_user_info&display=pc&state=1&redirect_uri=" . urlencode($this->request->domain() . "/thirdlogin/qq/callback"));
	}
	public function callback()
	{
		if (!input("?get.code")) {
			echo 'Code missing';
		} else {
			$code = input("get.code");
			$url = $this->accesstokenUrl . "?grant_type=authorization_code&client_id=" . $this->appid . "&client_secret=" . $this->appkey . "&code=" . $code . "&redirect_uri=" . urlencode($this->request->domain() . "/thirdlogin/qq/callback");
			$ret = (httpGetFull($url));
			if (strpos($ret, "callback") !== false) {
				$this->login();
				die;
			} else {
				$retArr = array();
				parse_str($ret, $retArr);
				$access_token = $retArr['access_token'];
				//$refresh_token = $retArr ['refresh_token'];
				//$expires_in = $retArr ['expires_in'];
				$url = $this->openidUrl . "?access_token=" . $access_token;
				$ret = httpGetFull($url);
				if (strpos($ret, "callback") === false) {
					$this->login();
					die;
				} else {
					$ret = str_replace("callback(", "", $ret);
					$ret = str_replace(");", "", $ret);
					$retObj = json_decode($ret);

					$openid = $retObj->openid;
					$url = $this->userinfoUrl . "?access_token=" . $access_token . "&oauth_consumer_key=" . $this->appid . "&openid=" . $openid;
					$retObj = json_decode(httpGetFull($url));
					//print_r($retObj);die;
					if ($retObj->ret !== 0) {
						$this->login();
						die;
					} else {
						print_r($retObj);
					}
				}
			}
		}
	}
}
