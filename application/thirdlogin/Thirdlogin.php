<?php

namespace app\thirdlogin;

use \think\Request;
use \think\View;
use \think\Controller;

class Thirdlogin extends Controller
{
	protected $request;
	protected $view;
	public function __construct()
	{
		$configs = db("conf")->select();
		$c = [];
		foreach ($configs as $config) {
			$c[$config['conf_key']] = $config['conf_value'];
		}
		config($c);

		$this->request = Request::instance();
		$this->view = new View();
	}
	public function _empty()
	{
		$this->error("Action Error");
	}
}
