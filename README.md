# FuckAdmin
<img src="https://svg.hamm.cn?key=gitee&value=star&project=hamm/FuckAdmin">
<img src="https://svg.hamm.cn?key=gitee&value=fork&project=hamm/FuckAdmin">
<img src="https://svg.hamm.cn?key=gitee&value=watch&project=hamm/FuckAdmin">
<img src="https://svg.hamm.cn?key=gitee&value=commit&project=hamm/FuckAdmin">
<img src="https://svg.hamm.cn/?key=框架&value=ThinkPHP5"/>
<img src="https://svg.hamm.cn/?key=数据库&value=MySQL5.5+"/>
<img src="https://svg.hamm.cn/?key=运行时&value=PHP7.1+"/>

### 介绍

FuckAdmin，基于ThinkPHP+VUE的后台管理二次开发脚手架 集成了微信接入、微信(登录)支付、第三方登录、oAuth2.0，权限管理，用户(组)管理，JQWeUI，菜单管理，节点管理，访问日志，访问统计，API生成，后台代码生成，Excel数据导出等常用功能，是轻量级后端脚手架。QQ群: 973087692


### 推荐配置
```
LNMP环境：

- PHP7.1+
- Nginx
- CentOS7
- MySQL5.6+
```
### 特色功能
```
基于用户组的RBAC权限控制
基于VUE+LayUI的后端管理
基于jQueryWeUI+VUE的移动Web页面管理
微信接入 登录 支付
第三方登录
oAuth2.0授权(优化中)
后端代码生成 数据表与模型生成 API生成
Excel数据导出
```
### 感谢(以下排名不分先后)
``` 
Vue.js / LayUI / jQueryWeUI / ThinkPHP5  
```

### 参与贡献
```
1. Fork 本仓库
2. 新建分支 添加或修改功能
3. 提交代码
4. 新建 Pull Request
```
### 晒个截图


![Demo](https://images.gitee.com/uploads/images/2019/1203/103016_eee24d05_145025.png "Demo")
