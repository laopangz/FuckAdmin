

## 获取我的个人资料

### 1) 请求地址

> /user/getMyInfo

### 2) 调用方式：HTTP post

### 3) 接口描述：

* 获取我的个人资料

### 4) 请求参数:

#### POST参数:
|字段名称       |字段说明         |类型            |必填            |备注     |
| -------------|:--------------:|:--------------:|:--------------:| ------:|
|access_token||string|Y|-|

### 5) 请求返回结果:

``` 
{
    "code": 0,
    "msg": "success",
    "data": {
        "user_id": 1,
        "user_account": "admin",
        "user_name": "超级管理员",
        "user_idcard": "500000000000000000",
        "user_truename": "Hamm",
        "user_email": "admin@hamm.cn",
        "user_money": "0.00",
        "user_group": 1,
        "user_wechat": 0,
        "user_wxapp": 0,
        "user_qq": 0,
        "user_createtime": 0,
        "user_updatetime": 0
    }
}
```

### 6) 请求返回结果参数说明:

|字段名称       |字段说明         |类型            |必填            |备注     |
| -------------|:--------------:|:--------------:|:--------------:| ------:|
|code|错误代码|int|Y|-|
|msg|错误信息|string|Y|-|
|data|返回数据|object|Y|-|
|----user_id||string|Y|-|
|----user_account||string|Y|-|
|----user_name||string|Y|-|
|----user_idcard||string|Y|-|
|----user_truename||string|Y|-|
|----user_email||string|Y|-|
|----user_money||string|Y|-|
|----user_group||string|Y|-|
|----user_wechat||string|Y|-|
|----user_wxapp||string|Y|-|
|----user_qq||string|Y|-|
|----user_createtime||string|Y|-|
|----user_updatetime||string|Y|-|

