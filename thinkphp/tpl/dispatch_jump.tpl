<html>
    <head>
        <title>温馨提示</title>
        <meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-title" content="Hamm">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <style type="text/css">
        body, html {
            width: 100%;
            height: 100%;
            overflow: hidden;
            padding: 0px;
            margin: 0px;
        }
        #svgContainer {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: 10%;
        }
        .tips{
            color: <?php echo $code==1?"green":"orangered";?>;
            position: absolute;
            bottom: 40px;
            text-align: center;
            left: 0;
            right: 0;
            font-size: 18px;
        }
        </style>
    </head>
    
    <body>
        <div class="bg-fixed" style="background:#fff url();background-attachment:fixed;z-index:-1;position:fixed;left:0;right:0;top:0;bottom:0;"></div>
        <script src="/error/bodymovin.js"></script>
        <script src="/error/data.js"></script>
        <div id="svgContainer"></div>
        <div class="tips"><?php echo $msg?$msg:"哦豁，出了点问题";?></div>
    </body>
    <script>
    var svgContainer = document.getElementById('svgContainer');
    var animItem = bodymovin.loadAnimation({
        wrapper: svgContainer,
        animType: 'svg',
        loop: true,
        animationData: JSON.parse(animationData)
    });
    setTimeout(function(){
        location.replace('<?php echo($url);?>');
    },3000);
    </script>

</html>